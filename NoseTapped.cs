private async void NoseTapped(object sender, TappedRoutedEventArgs e) 
{ 
            var assets = await Package.Current.InstalledLocation.GetFolderAsync("assets"); 
            var buzzer = await assets.GetFileAsync("buzzer.wav"); 
            var stream = await buzzer.OpenAsync(FileAccessMode.Read); 
 
            _buzzer = new MediaElement(); 
            _buzzer.SetSource(stream, buzzer.ContentType); 
}