     public void LookAt(Point pos) 
        { 
            // Delta X and Y 
            double dx = pos.X - Center.X; 
            double dy = pos.Y - Center.Y; 
 
            // distance to position 
            double distance = Math.Sqrt(dx * dx  + dy * dy ); 
 
 
            // calculate  center of pupil 
            double scale = (Radius-PupilRadius)  / distance; 
 
            double x = dx; 
            double y = dy; 
 
            if (distance > Radius-PupilRadius) 
            { 
                x = dx * scale; 
                y = dy * scale * 2; 
            } 
            x += Radius - PupilRadius; 
             
            MovePupil(x, y); 
        }